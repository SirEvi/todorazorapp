﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RazorTodoApp.Models
{
    public class Todo
    {
        [Key]
        public int Id { get; set; }

        public DateTime Date { get; set; } = DateTime.Now;

        [Required]
        [MaxLength(200)]
        public string Task { get; set; }

        [Required]
        [MaxLength(50)]
        public string EstimatedTime { get; set; }

        [Required]
        [MaxLength(50)]
        public string Person { get; set; }

        [MaxLength(50)]
        [Column(TypeName = "varChar(15)")]
        public string Status { get; set; } = "InProgress";
    }
}
