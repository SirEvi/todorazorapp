﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using RazorTodoApp.Models;

namespace RazorTodoApp.Pages.TodoList
{
    public class EditModel : PageModel
    {
        private TodoDbContext _db;

        public EditModel(TodoDbContext db)
        {
            _db = db;
        }

        [BindProperty]
        public Todo Todo { get; set; }
        public async Task OnGet(int id)
        {
            Todo = await _db.Todos.FindAsync(id);
        }
        public async Task<IActionResult> OnPost()
        {
            if (ModelState.IsValid)
            {
                var TodoFromDb = await _db.Todos.FindAsync(Todo.Id);
                TodoFromDb.Date = Todo.Date;
                TodoFromDb.Task = Todo.Task;
                TodoFromDb.EstimatedTime = Todo.EstimatedTime;
                TodoFromDb.Person = Todo.Person;
                TodoFromDb.Status = Todo.Status;

                await _db.SaveChangesAsync();
                return RedirectToPage("Index");
            }
            return RedirectToPage();
        }
    }
}