﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using RazorTodoApp.Models;

namespace RazorTodoApp.Pages.TodoList
{
    public class CreateModel : PageModel
    {
        private TodoDbContext _db;

        public CreateModel(TodoDbContext db)
        {
            _db = db;
        }

        [BindProperty]
        public Todo Todo { get; set; }

        public void OnGet()
        {

        }

        public async Task<IActionResult> OnPost()
        {
            if (ModelState.IsValid)
            {
                await _db.Todos.AddAsync(Todo);
                await _db.SaveChangesAsync();
                return RedirectToPage("Index");
            }
            else
            {
                return Page();
            }
        }
    }
}