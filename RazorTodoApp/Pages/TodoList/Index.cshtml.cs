﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using RazorTodoApp.Models;

namespace RazorTodoApp.Pages.TodoList
{
    public class IndexModel : PageModel
    {
        private readonly TodoDbContext _db;

        public IndexModel(TodoDbContext db)
        {
            _db = db;
        }

        public IEnumerable<Todo> Todos { get; set; }

        public async Task OnGet()
        {
            Todos = await _db.Todos.ToListAsync();
        }

        public async Task<IActionResult> OnPostDelete(int id)
        {
            var todo = await _db.Todos.FindAsync(id);
            if (todo == null)
            {
                return NotFound();
            }
            else
            {
                _db.Todos.Remove(todo);
                await _db.SaveChangesAsync();

                return RedirectToPage("Index");
            }
        }
    }
}