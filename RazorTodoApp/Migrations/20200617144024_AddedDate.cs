﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RazorTodoApp.Migrations
{
    public partial class AddedDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Status",
                table: "Todos",
                type: "varChar(15)",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varChar(15)",
                oldMaxLength: 50);

            migrationBuilder.AddColumn<DateTime>(
                name: "Date",
                table: "Todos",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Date",
                table: "Todos");

            migrationBuilder.AlterColumn<string>(
                name: "Status",
                table: "Todos",
                type: "varChar(15)",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varChar(15)",
                oldMaxLength: 50,
                oldNullable: true);
        }
    }
}
